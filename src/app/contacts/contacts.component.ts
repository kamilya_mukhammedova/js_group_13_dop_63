import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  contact = '';
  preloader = false;
  @ViewChild('userContactInput') userContactInput!: ElementRef;

  constructor(
    private http: HttpClient,
    @Inject(DOCUMENT) private document: Document,
  ) {}

  ngOnInit(): void {
    this.preloader = true;
    this.http.get<string>('https://kamilya-61357-default-rtdb.firebaseio.com/pages/contacts.json')
      .pipe(map(result => {
        return result;
      }))
      .subscribe(contact => {
        this.contact = contact;
        this.preloader = false;
      })
  }

  addContact() {
    if(this.userContactInput.nativeElement.value !== '') {
      this.preloader = true;
      const userContact = JSON.stringify(this.userContactInput.nativeElement.value);
      this.http.put('https://kamilya-61357-default-rtdb.firebaseio.com/pages/contacts.json', userContact)
        .subscribe(() => {
          this.document.defaultView!.location.reload();
          this.preloader = false;
        });
    } else {
      alert('Please enter your contact');
    }
  }
}
