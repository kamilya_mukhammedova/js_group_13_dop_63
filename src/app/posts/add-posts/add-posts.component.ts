import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Post } from '../../shared/post.model';

@Component({
  selector: 'app-add-posts',
  templateUrl: './add-posts.component.html',
  styleUrls: ['./add-posts.component.css']
})
export class AddPostsComponent implements OnInit {
  title = '';
  description = '';
  formTitle = '';
  date = '';
  postId = '';

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      if(params['id']) {
        this.formTitle = 'Edit post';
        this.postId = params['id'];
        this.getPostData(this.postId);
      } else {
        this.formTitle = 'Add new post';
      }
    });
  }

  getPostData(id: string) {
    this.http.get<Post>(`https://kamilya-61357-default-rtdb.firebaseio.com/myPosts/${id}.json`)
      .pipe(map(result => {
        return result;
      }))
      .subscribe(post => {
        this.title = post.title;
        this.description = post.description;
        this.date = post.date.toString();
      });
  }

  savePost() {
    if(this.formTitle === 'Add new post') {
      if(this.description !== '' && this.title !== '') {
        const date = new Date();
        const body = {date: date.toString(), description: this.description, title: this.title};
        this.http.post('https://kamilya-61357-default-rtdb.firebaseio.com/myPosts.json', body)
          .subscribe(() => {
            void this.router.navigate(['/']);
          });
      } else {
        alert('You need to fill all the fields!');
      }
    } else {
      const body = {date: this.date, description: this.description, title: this.title};
      this.http.put(`https://kamilya-61357-default-rtdb.firebaseio.com/myPosts/${this.postId}.json`, body)
        .subscribe(() => {
          void this.router.navigate(['/']);
        });
    }
  }
}
