import { Component, OnInit } from '@angular/core';
import { Post } from '../shared/post.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  postsArray: Post[] = [];
  preloader = false;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.preloader = true;
    this.http.get<{ [id: string]: Post }>('https://kamilya-61357-default-rtdb.firebaseio.com/myPosts.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const postData = result[id];
          return new Post(id, postData.date, postData.description, postData.title);
        });
      }))
      .subscribe(posts => {
        this.postsArray = posts;
        this.preloader = false;
      });
  }
}
