import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PostDetailComponent } from './posts/post-detail/post-detail.component';
import { AddPostsComponent } from './posts/add-posts/add-posts.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';

const routes: Routes = [
  {path: '', component: PostsComponent},
  {path: 'posts', component: PostsComponent, children: [
      {path: 'add', component: AddPostsComponent},
      {path: ':id', component: PostDetailComponent},
      {path: ':id/edit', component: AddPostsComponent},
    ]},
  {path: 'about', component: AboutComponent},
  {path: 'contacts', component: ContactsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
