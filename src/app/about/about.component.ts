import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  message = '';
  preloader = false;
  @ViewChild('commentInput') commentInput!: ElementRef;

  constructor(
    private http: HttpClient,
    @Inject(DOCUMENT) private document: Document,
  ) {}

  ngOnInit(): void {
    this.preloader = true;
    this.http.get<string>('https://kamilya-61357-default-rtdb.firebaseio.com/pages/about.json')
      .pipe(map(result => {
        return result;
      }))
      .subscribe(message => {
        this.message = message;
        this.preloader = false;
      })
  }

  addComment() {
    if(this.commentInput.nativeElement.value !== '') {
      this.preloader = true;
      const userComment = JSON.stringify(this.commentInput.nativeElement.value);
      this.http.put('https://kamilya-61357-default-rtdb.firebaseio.com/pages/about.json', userComment)
        .subscribe(() => {
          this.document.defaultView!.location.reload();
          this.preloader = false;
        });
    } else {
      alert('Please enter your comment');
    }
  }
}
